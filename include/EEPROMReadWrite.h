#ifndef EEPROMReadWrite_h
    #define EEPROMReadWrite_h
    #define EEPROM_SIZE 64


    #if ARDUINO >= 100
        #include "Arduino.h"
        #include "EEPROM.h"
    #else
        #include "WProgram.h"
        #include "pins_arduino.h"
        #include "WConstants.h"
    #endif


    class EEPROMReadWrite {
        public:
            EEPROMReadWrite(); // constructor
            void writeString(String str);
            String readString();
        private:
            int _addr = 0;
            char _eeprom_read_val_char[64] = "";
            String _eeprom_string_val;
            String _eeprom_read_val_str = "";
    };
#endif

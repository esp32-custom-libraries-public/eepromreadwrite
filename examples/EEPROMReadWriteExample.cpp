#include <Arduino.h>
#include <EEPROMReadWrite.h>

EEPROMReadWrite EERW = EEPROMReadWrite();
const int COMMON_DELAY = 1000;

String ee_read_str;
String ee_write_str = "Hello world."; // max 64 characters

void read_EEPROM_val(String label) {
    Serial.println(F("READING ..."));
    Serial.print(F("\t"));
    Serial.print(label);
    Serial.print(F(" EEPROM value = "));
    ee_read_str = EERW.readString();
    delay(COMMON_DELAY);

    Serial.println(ee_read_str);
    Serial.print(F("\tlength: "));
    Serial.print(ee_read_str.length());
    Serial.println("/64 characters");
    delay(COMMON_DELAY);
}

void write_EEPROM_val() {
    Serial.println(F("WRITING ..."));
    EERW.writeString(ee_write_str);
    delay(COMMON_DELAY);
}

void setup() {
    Serial.begin(9600);
    Serial.println(F("\n\n"));

    delay(2000);

    read_EEPROM_val("previous");
    write_EEPROM_val();
    read_EEPROM_val("new");

    Serial.println(F("DONE."));
}

void loop() {}

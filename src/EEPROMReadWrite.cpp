#include "EEPROMReadWrite.h"


EEPROMReadWrite::EEPROMReadWrite() {} // constructor


void EEPROMReadWrite::writeString(String str) {
    _eeprom_string_val = str;
    if (!EEPROM.begin(EEPROM_SIZE)) {
        delay(1000000);
    }
    // writing byte-by-byte to EEPROM
    for (int i = 0; i < EEPROM_SIZE; i++) {
        if (i < _eeprom_string_val.length()) {
            EEPROM.write(_addr, _eeprom_string_val[i]);
        } else {
            EEPROM.write(_addr, ' ');
        }
        _addr += 1;
    }
    EEPROM.commit();
    return;
}


String EEPROMReadWrite::readString() {
    if (!EEPROM.begin(EEPROM_SIZE)) {
        delay(1000000);
    }
    // reading byte-by-byte from EEPROM
    for (int i = 0; i < EEPROM_SIZE; i++) {
        byte readValue = EEPROM.read(i);

        if (readValue == 0) {
            break;
        }
        char readValueChar = char(readValue);
        _eeprom_read_val_char[i] = readValueChar;
        // there are 4 strange characters at the end of the
        // character array that need to be left off and the
        // array needs to be converted to a String. trim
        // whitespace before returning
        _eeprom_read_val_str = String(_eeprom_read_val_char).substring(0, EEPROM_SIZE-1);
        _eeprom_read_val_str.trim();
    }
    return String(_eeprom_read_val_str);
}
